# Welcome to my testing lab

Here you can find all sorts of programs I have made for testing purposes. 
Feel free to use any of this code for your own needs!

# Index

### `destruction1.sh`: Run this at your own risk

### `game1.py`: A tkinter game/program where you can draw on a canvas with different colours

### `portscanner.py`: A port scanning application built in python

### `arpscanner.py`: A arp (network device discovery) application built in python using scapy (in development)

### `nmapwrapper.py`: A nmap wrapper built in shell script (bash)
